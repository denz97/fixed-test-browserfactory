import firstSuites.BaseTest;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pages.firstPageObject.GoogleResultPage;
import pages.firstPageObject.GoogleSearchPage;

import static pages.firstPageObject.GoogleResultPage.URL_AUTOPRACTICE;


public class MyFirstTest extends BaseTest {

    GoogleSearchPage googleSearchPage;
    GoogleResultPage googleResultPage;



    @BeforeMethod
    public void beforeMethod(){
        googleSearchPage = new GoogleSearchPage(getDriver());
        googleResultPage = new GoogleResultPage(getDriver());
    }
    @Test
    public void myFirstTest1(){
        googleSearchPage.open();
        googleSearchPage.search("automationpractice");
        googleResultPage.openUrlByName();
        Assert.assertEquals(URL_AUTOPRACTICE, getDriver().getCurrentUrl());
    }
}