package utils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class BrowserFactory {
    public static WebDriver getDriver (String name){
        switch (name){
            case "FIREFOX":
                System.setProperty("webdriver.gecko.driver","src/test/resources/drivers/geckodriver.exe");
                return new FirefoxDriver();
            case "CHROME":
                System.setProperty("webdriver.chrome.driver","src/test/resources/drivers/chrome.exe");
                return new ChromeDriver();
            case "EDGE":
                System.setProperty("webdriver.edge.driver","src/test/resources/drivers/msedgedriver1.exe");
                return new EdgeDriver();
            default:
                System.setProperty("webdriver.gecko.driver","src/test/resources/drivers/geckodriver.exe");
                return new FirefoxDriver();
        }
    }
}
