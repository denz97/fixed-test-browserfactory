package pages.firstPageObject;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pages.BasePage;
import utils.Waiters;

public class GoogleResultPage extends BasePage {
    public GoogleResultPage(WebDriver driver) {
        super(driver);
    }
    public static final String URL_AUTOPRACTICE = "http://automationpractice.com/index.php";
    public static final String SEARCH_LINK_ELEMENT = ".//h3[text()='Automation Practice']";

    @FindBy(xpath = SEARCH_LINK_ELEMENT)
    private WebElement searchLink;

    public void openUrlByName() {
        searchLink.click();
        Waiters.waitForUrl(driver, Waiters.TIME_FIVE, URL_AUTOPRACTICE);
    }
}
